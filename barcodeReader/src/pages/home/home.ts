import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';
import {BarcodeScanner, BarcodeScannerOptions} from '@ionic-native/barcode-scanner'
import {NFC, Ndef} from '@ionic-native/nfc'
import { Platform } from 'ionic-angular/platform/platform';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit{

  options: BarcodeScannerOptions;
  results: {};
  data: string[] = [];
  tags: Ndef[];
  value: string;
  constructor(private nfc:NFC, private ndef: Ndef, private barcode: BarcodeScanner, public navCtrl: NavController, public platform:Platform) {

  }


  ngOnInit(){
    this.platform.ready().then(() => {
      this.loginNFC();
    })

    this.value = this.navParams.get('value');
    console.log("ID: ", this.value);
  }


  loginNFC() {
    this.NFC.addNdefListener().subscribe(scannedTag => {
      this.tags = scannedTag.tag.ndefMessage;
      console.log(this.tags);

      for (let entry of this.tags) {
        this.data.push(this.bytesToString(entry["payload"].splice(3)));
        console.log(this.bytesToString(entry["payload"].splice(3)));
      }
      this.navCtrl.setRoot(NfcDara, { value: this.data[0] });
    });
}



bytesToString(bytes) {
  // based on http://ciaranj.blogspot.fr/2007/11/utf8-characters-encoding-in-javascript.html
  var resultOfNFC = "";
  var i, c, c1, c2, c3;
  i = c = c1 = c2 = c3 = 0;

  // Perform byte-order check.
  if (bytes.length >= 3) {
    if ((bytes[0] & 0xef) == 0xef && (bytes[1] & 0xbb) == 0xbb && (bytes[2] & 0xbf) == 0xbf) {
      // stream has a BOM at the start, skip over
      i = 3;
    }
  }

  while (i < bytes.length) {
    c = bytes[i] & 0xff;

    if (c < 128) {

      resultOfNFC += String.fromCharCode(c);
      i++;

    } else if ((c > 191) && (c < 224)) {

      if (i + 1 >= bytes.length) {
        throw "Un-expected encoding error, UTF-8 stream truncated, or incorrect";
      }
      c2 = bytes[i + 1] & 0xff;
      resultOfNFC += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
      i += 2;

    } else {

      if (i + 2 >= bytes.length || i + 1 >= bytes.length) {
        throw "Un-expected encoding error, UTF-8 stream truncated, or incorrect";
      }
      c2 = bytes[i + 1] & 0xff;
      c3 = bytes[i + 2] & 0xff;
      resultOfNFC += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      i += 3;

    }
  }
  return resultOfNFC;
}


  async scanBarcode(){

    this.options = {
      prompt: 'Scan a barcode to see the result'
    }

    const results = await this.barcode.scan(this.options);
    console.log(this.results);
  }


  async encodeData(){
    const result = await this.barcode.encode(this.barcode.Encode.TEXT_TYPE, 'http://seeifitworks.com')
  }

  // async readNFCTag(){

  //   this.nfc.addNdefListener(() => {
  //     console.log('successfully attached ndef listener');
  //   }, (err) => {
  //     console.log('error attaching ndef listener', err);
  //   }).subscribe((event) => {
  //     console.log('received ndef message. the tag contains:', event.tag);
  //     console.log('decoded tag id', this.nfc.bytesToHexString(event.tag.id));

  //     let message = this.ndef.textRecord('Finally it happens');
  //     this.nfc.share([message]).then(onSuccess).catch(onError);
  //   });

  //}



}
